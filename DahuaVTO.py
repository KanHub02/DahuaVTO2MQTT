#!/usr/bin/env python3
import json
import logging
import os
import sys
from time import sleep

from prometheus_client import CollectorRegistry, start_http_server

from clients.DahuaClient import DahuaClient
from clients.MQTTClient import MQTTClient
from common.consts import (
    CONFIG_FILE,
    DEFAULT_EXPORTER_PORT,
    UNOFFICIAL_VERSION,
    VERSION_FILE,
)

DEBUG = str(os.environ.get("DEBUG", False)).lower() == str(True).lower()

log_level = logging.DEBUG if DEBUG else logging.INFO

root = logging.getLogger()
root.setLevel(log_level)

stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(log_level)
formatter = logging.Formatter("%(asctime)s %(levelname)s %(name)s %(message)s")
stream_handler.setFormatter(formatter)
root.addHandler(stream_handler)

_LOGGER = logging.getLogger(__name__)


class DahuaVTOManager:
    def __init__(self):
        self._registry: CollectorRegistry | None = None

        self._version: str | None = None

        self._mqtt_client: MQTTClient | None = None
        self._dahua_client: DahuaClient | None = None

        self._exporter_port: int | None = None

    def initialize(self):
        self._load_configuration()

        start_http_server(self._exporter_port)

        self._mqtt_client = MQTTClient(self._version, self._registry, self._config)
        self._dahua_client = DahuaClient(self._version, self._registry, self._config)

        self._mqtt_client.initialize(self._dahua_client.outgoing_events)
        self._dahua_client.initialize(self._mqtt_client.outgoing_events)

        while True:
            sleep(1)

    def _load_configuration(self):
        if os.path.exists(VERSION_FILE):
            with open(VERSION_FILE) as file:
                version_data = json.load(file)
                self._version = version_data.get("version")
        else:
            self._version = UNOFFICIAL_VERSION

        with open(CONFIG_FILE) as file:
            self._config = json.load(file)

        _LOGGER.info(f"Loading DahuaVTO2MQTT configuration, Version: {self._version}")

        self._exporter_port = int(
            os.environ.get("EXPORTER_PORT", str(DEFAULT_EXPORTER_PORT))
        )

        self._registry = CollectorRegistry()


manager = DahuaVTOManager()
manager.initialize()
