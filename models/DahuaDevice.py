import logging
import os

from requests.auth import HTTPDigestAuth

from common.consts import PROTOCOLS
from common.enums import DahuaConfig, DahuaRPC

_LOGGER = logging.getLogger(__name__)


class DahuaDevice:
    hostname: str | None
    username: str | None
    password: str | None
    is_ssl: bool | None
    config: dict[DahuaRPC, dict]
    version: str | None
    build_date: str | None
    type: str | None
    serial_number: str | None
    access_control_token: int | None
    hold_time: float
    auth: HTTPDigestAuth | None

    def __init__(self, hostname, is_ssl, username, password):
        self.hostname = hostname
        self.is_ssl = is_ssl

        self.username = username
        self.password = password

        self.config = {}
        self.version = None
        self.build_date = None
        self.type = None
        self.serial_number = None
        self.access_control_token = None
        self.hold_time = 0

        self._lock_status: dict[int, bool] = {}

        self._auth = HTTPDigestAuth(self.username, self.password)
        self._base_url = f"{PROTOCOLS[self.is_ssl]}://{self.hostname}/cgi-bin/"

        self._config_processors = {
            str(DahuaRPC.GET_DEVICE_TYPE): self._update_device_type,
            str(DahuaRPC.ACCESS_CONTROL_FACTORY_INSTANCE): self._update_access_control_factory_instance,
            str(DahuaRPC.GET_SOFTWARE_VERSION): self._update_version,
            str(DahuaRPC.GET_SERIAL_NUMBER): self._update_device_serial_number,
            f"{DahuaRPC.GET_CONFIG}|{DahuaConfig.ACCESS_CONTROL}": self._update_hold_time
        }

    @property
    def base_url(self) -> str:
        return self._base_url

    @property
    def port(self) -> int:
        return 5000

    @property
    def auth(self) -> HTTPDigestAuth:
        return self._auth

    def set_lock(self, door: int, state: bool) -> None:
        self._lock_status[door] = state

    def is_locked(self, door: int) -> bool:
        is_locked = self._lock_status.get(door, False)

        return is_locked

    def update(self, endpoint: DahuaRPC, data: dict | str, sub_param: str | None = None):
        if sub_param is None:
            _LOGGER.debug(f"Set config item '{endpoint}', Data: {data}")

        else:
            _LOGGER.debug(f"Set config item '{endpoint}', Sub action: {sub_param}, Data: {data}")

        device_config = self.config[endpoint] if endpoint in self.config else {}

        config_id = str(endpoint)

        if sub_param is None:
            device_config.update(data)

        else:
            config_id = f"{config_id}|{sub_param}"
            sub_device_config = device_config[sub_param] if sub_param in device_config else {}
            sub_device_config.update(data)

            device_config[sub_param] = sub_device_config

        self.config[endpoint] = device_config

        if config_id in self._config_processors:
            process = self._config_processors[config_id]

            process(data)

    def _update_hold_time(self, data: dict):
        access_control_table = data.get("table", [])

        for item in access_control_table:
            access_control = item.get('AccessProtocol')

            if access_control == 'Local':
                hold_time = item.get('UnlockReloadInterval')

                self.hold_time = hold_time

                _LOGGER.info(f"Hold time: {self.hold_time}")

    def _update_version(self, data: dict):
        version_details = data.get("version", {})
        build_date = version_details.get("BuildDate")
        version = version_details.get("Version")

        self.version = version
        self.build_date = build_date

        _LOGGER.info(f"Version: {self.version}")
        _LOGGER.info(f"Build Date: {self.build_date}")

    def _update_device_type(self, data: dict):
        device_type = data.get("type")

        self.type = device_type

        _LOGGER.info(f"Type: {self.type}")

    def _update_access_control_factory_instance(self, data: dict):
        token = data.get("instance")

        self.access_control_token = token

        _LOGGER.info(f"Access Control Instance ID: {self.access_control_token}")

    def _update_device_serial_number(self, data: dict):
        self.serial_number = data.get("sn")

        _LOGGER.info(f"Serial Number: {self.serial_number}")

    @staticmethod
    def load_from_env_var():
        hostname = os.environ.get('DAHUA_VTO_HOST')
        is_ssl = str(os.environ.get('DAHUA_VTO_SSL', False)).lower() == str(True).lower()

        username = os.environ.get('DAHUA_VTO_USERNAME')
        password = os.environ.get('DAHUA_VTO_PASSWORD')

        device = DahuaDevice(hostname, is_ssl, username, password)

        return device
